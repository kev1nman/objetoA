<?php include "partials/headerHTML.php"; include "partials/nav.php"; ?>

<section class="header" id="porque">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 noP">
                <div class="slider" id="page">
                    <ul class="cb-slideshow">
                        <li><span></span><div><h3>re·lax·a·tion</h3></div></li>
                        <li><span></span><div><h3>qui·e·tude</h3></div></li>
                        <li><span></span><div><h3>re·lax·a·tion</h3></div></li>
                        <li><span></span><div><h3>qui·e·tude</h3></div></li>
                        <li><span></span><div><h3>re·lax·a·tion</h3></div></li>
                        <li><span></span><div><h3>qui·e·tude</h3></div></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="sect1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>¿ Por qué</h1>
                <h3 class="marginLeft">consultar ?</h3>
                <div class="separador">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-3 linea1"></div>
                            <div class="col-xs-9 linea2"></div>
                        </div>
                    </div>
                </div>
                <p>
                    When I first got into the online advertising business, I was looking for the magical combination that would put my website into the top search engine rankings, catapult me to the forefront of the minds or individuals looking to buy my product, and generally make me rich beyond my wildest dreams! After succeeding in the business for this long, I’m able to look back on my old self with this kind of thinking and shake my head. If you’re reading this article and you’ve come this far, you’re probably looking for the magic answer yourself.
                </p>
            </div>
        </div>
    </div>
</section>


<section class="sect2" id="servicios">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><span class="theme1">Ser</span>vicios</h1>
            </div>
            <div class="box servicio serviciolg">
                <div class="row">
                    <div class="col-md-5">
                        <div class="img">
                            
                        </div>
                    </div>
                    <div class="col-md-7 info">
                        <h3>Clínica infantil</h3>
                        <p>
                            Espacio terapéutico a través del juego, el dibujo y el uso libre de la palabra para niños y niñas entre los 3 y 12 años.
                        </p>
                        <p class="indentado">
                            “…Un niño llega al consultorio de un analista por las resonancias que genera en un adulto…” (Alba Flesler)
                        </p>
                        <a class="btn btn-primary btn-servicio" href="servicio.php">Me interesa</a>
                    </div>
                </div>
            </div>
            <div class="box servicio serviciolg">
                <div class="row">
                    <div class="col-md-5">
                        <div class="img">
                            
                        </div>
                    </div>
                    <div class="col-md-7 info">
                        <h3>Clínica infantil</h3>
                        <p>
                            Espacio terapéutico a través del juego, el dibujo y el uso libre de la palabra para niños y niñas entre los 3 y 12 años.
                        </p>
                        <p class="indentado">
                            “…Un niño llega al consultorio de un analista por las resonancias que genera en un adulto…” (Alba Flesler)
                        </p>
                        <a class="btn btn-primary btn-servicio" href="servicio.php">Me interesa</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 noP">
                <div class="box servicio serviciosm">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="img2">
                                
                            </div>
                        </div>
                        <div class="col-md-7 info">
                            <h3>Clínica juvenil</h3>
                            <p>
                                Espacio de trabajo clínico, interrogación y reflexión para jóvenes entre 13 y 18 años.                            </p>
                            <p class="indentado">
                                “Dad palabra al dolor: el dolor que no habla gime en el corazón hasta que lo rompe” (William Shakespeare)                            </p>
                            <a class="btn btn-primary btn-servicio" href="servicio.php">Me interesa</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 noP">
                <div class="box servicio serviciosm">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="img3">

                            </div>
                        </div>
                        <div class="col-md-7 info">
                            <h3>Clínica adultos</h3>
                            <p>
                                Espacio de escucha, interpelación y análisis para mayores de 19 años.
                            </p>
                            <p class="indentado">
                                "Quizá sólo buscamos a lo largo de la vida la gran aflicción de ser uno mismo y nada más" (Louis Céline)                            </p>
                            <a class="btn btn-primary btn-servicio" href="servicio.php">Me interesa</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="sect3">
    <div class="container-fluid">
        <div class="row">
            
            <div class="col-md-6 noP">
                <div class="box insti">
                    <h1>Supervision</h1>
                    <h3>Institucional</h3>
                    <p>
                        They are grilling celebrities in their own right. You’ve seen them on TV and you see their cookbooks lined along the shelves of your local bookstore. They may have different backgrounds and a variety of cooking styles, but just like you, they all share the same passion and that is for grilling and barbecues.                        
                    </p>
                </div>
            </div>
            <div class="col-md-6 noP">
                <div class="box pro">
                    <h1>Supervision</h1>
                    <h3>Profesional</h3>
                    <p>
                        They are grilling celebrities in their own right. You’ve seen them on TV and you see their cookbooks lined along the shelves of your local bookstore. They may have different backgrounds and a variety of cooking styles, but just like you, they all share the same passion and that is for grilling and barbecues.                        
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="sect4" id="profesionales">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <h1><span class="theme1">Pro</span>fesionales</h1>
            </div>
            <div class="contentPro">
                <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-6 pro1 noP">
                                
                            </div>
                            <div class="col-md-6 box">
                                <h4>Gustavo <span>Bustos Gajardo</span></h4>
                                <p>
                                    Socio Fundador de Objeto a. Psicólogo, Universidad Academia de Humanismo Cristiano; Magíster © en Filosofía, especialidad en Filosofía Francesa Contemporánea, Universidad de Chile.
                                </p>
                                <ul>
                                    <li><span class="ion-social-whatsapp ws"></span> (09) 9 638 0703</li>
                                    <li><span class="ion-email mail"></span> gbustosg@objetoa.cl</li>
                                    <li><span class="ion-social-linkedin linked"></span> gbustosg@objetoa.cl</li>
                                </ul>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <a href="#" class="btn btn-primary btn-border" data-toggle="modal" data-target="#pro1"> Leer mas</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-6 pro2 noP">
                                
                            </div>
                            <div class="col-md-6 box">
                                <h4>Lorena <span>Osorio Clavijo</span></h4>
                                <p>
                                    Socia Fundadora y Directora Clínica de Objeto a. Psicóloga con orientación psicoanalítica. Su especialización en la escucha clínica se ha sostenido a partir del desarrollo de su trabajo.                            </p>
                                <ul>
                                    <li><span class="ion-social-whatsapp ws"></span> (09) 7 624 0100</li>
                                    <li><span class="ion-email mail"></span> lorenaosorio@objetoa.cl</li>
                                    <li><span class="ion-social-linkedin linked"></span> lorenaosorio@objetoa.cl</li>
                                </ul>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <a href="#" class="btn btn-primary btn-border" data-toggle="modal" data-target="#pro2"> Leer mas</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>


<section class="sect5">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Preguntas</h1>
                <h3 class="marginLeft"> y respuestas</h3>
                <div class="separador">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-3 linea1"></div>
                            <div class="col-xs-9 linea2"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            
                        <div class="panel panel-default">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#uno" aria-expanded="true" aria-controls="collapseOne">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title text-left">
                                        
                                            <i class="ion-ios-arrow-down pull-right"></i>
                                            ¿ Writing A Good Headline For Your Advertisement ?
                                        
                                    </h4>
                                </div>
                            </a>
                            <div id="uno" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                        It is important that you buy links because the links are what get you the results that you want. The popularity of the links that are listed in the MTA directory is in fact one of the most important factors in the performance of the search engine. Links are important and this is why you have to purchase a link in order to bid on something and the best part is that a link will only cost you $1, which is nothing compared to what you would pay if you decided to do it through any other company or website.                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#dos" aria-expanded="true" aria-controls="collapseOne">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title text-left">
                                        
                                            <i class="ion-ios-arrow-down pull-right"></i>
                                            ¿ Writing A Good Headline For Your Advertisement ?
                                        
                                    </h4>
                                </div>
                            </a>
                            <div id="dos" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                        It is important that you buy links because the links are what get you the results that you want. The popularity of the links that are listed in the MTA directory is in fact one of the most important factors in the performance of the search engine. Links are important and this is why you have to purchase a link in order to bid on something and the best part is that a link will only cost you $1, which is nothing compared to what you would pay if you decided to do it through any other company or website.                                </div>
                            </div>
                        </div>




                        <div class="panel panel-default">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tres" aria-expanded="true" aria-controls="collapseOne">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title text-left">
                                        
                                            <i class="ion-ios-arrow-down pull-right"></i>
                                            ¿ Writing A Good Headline For Your Advertisement ?
                                        
                                    </h4>
                                </div>
                            </a>
                            <div id="tres" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                        It is important that you buy links because the links are what get you the results that you want. The popularity of the links that are listed in the MTA directory is in fact one of the most important factors in the performance of the search engine. Links are important and this is why you have to purchase a link in order to bid on something and the best part is that a link will only cost you $1, which is nothing compared to what you would pay if you decided to do it through any other company or website.                                </div>
                            </div>
                        </div>



                        
                        <div class="panel panel-default">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#cuatro" aria-expanded="true" aria-controls="collapseOne">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title text-left">
                                        
                                            <i class="ion-ios-arrow-down pull-right"></i>
                                            ¿ Writing A Good Headline For Your Advertisement ?
                                        
                                    </h4>
                                </div>
                            </a>
                            <div id="cuatro" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                        It is important that you buy links because the links are what get you the results that you want. The popularity of the links that are listed in the MTA directory is in fact one of the most important factors in the performance of the search engine. Links are important and this is why you have to purchase a link in order to bid on something and the best part is that a link will only cost you $1, which is nothing compared to what you would pay if you decided to do it through any other company or website.                                </div>
                            </div>
                        </div>





                        <div class="panel panel-default">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#cinco" aria-expanded="true" aria-controls="collapseOne">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title text-left">
                                        
                                            <i class="ion-ios-arrow-down pull-right"></i>
                                            ¿ Writing A Good Headline For Your Advertisement ?
                                        
                                    </h4>
                                </div>
                            </a>
                            <div id="cinco" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                        It is important that you buy links because the links are what get you the results that you want. The popularity of the links that are listed in the MTA directory is in fact one of the most important factors in the performance of the search engine. Links are important and this is why you have to purchase a link in order to bid on something and the best part is that a link will only cost you $1, which is nothing compared to what you would pay if you decided to do it through any other company or website.                                </div>
                            </div>
                        </div>





                        <div class="panel panel-default">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#seis" aria-expanded="true" aria-controls="collapseOne">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title text-left">
                                        
                                            <i class="ion-ios-arrow-down pull-right"></i>
                                            ¿ Writing A Good Headline For Your Advertisement ?
                                        
                                    </h4>
                                </div>
                            </a>
                            <div id="seis" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                        It is important that you buy links because the links are what get you the results that you want. The popularity of the links that are listed in the MTA directory is in fact one of the most important factors in the performance of the search engine. Links are important and this is why you have to purchase a link in order to bid on something and the best part is that a link will only cost you $1, which is nothing compared to what you would pay if you decided to do it through any other company or website.                                </div>
                            </div>
                        </div>
            
                
                    </div>
            </div>
        </div>
    </div>
</section>












<?php  include "modals.php"; include "partials/footer.php"; ?>