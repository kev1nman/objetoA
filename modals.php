<div class="modal fade" id="pro1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content profesionales">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <div class="row box">
              <div class="col-xs-12">
                  <div class="row">
                      <div class="col-md-6 pro1 noP">
  
                      </div>
                      <div class="col-md-6">
                        <div class="name">
                            <h4>Gustavo <span>Bustos Gajardo</span></h4>
                            <ul>
                                <li><span class="ion-social-whatsapp ws"></span> (09) 9 638 0703</li>
                                <li><span class="ion-email mail"></span> gbustosg@objetoa.cl</li>
                                <li><span class="ion-social-linkedin linked"></span> gbustosg@objetoa.cl</li>
                            </ul>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="col-xs-12">
                  <div class="row">
                      <div class="col-md-12">
                          <h4>Formación <span>profesional</span></h4>
                      </div>
                  </div>
              </div>
              <div class="col-xs-12">
                  <div class="row">
                      <div class="col-md-12">
                          <h4>Tra<span>yectoria</span></h4>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>




<div class="modal fade" id="pro2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content profesionales">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <div class="row box">
              <div class="col-xs-12">
                  <div class="row">
                      <div class="col-md-6 pro2 noP">
  
                      </div>
                      <div class="col-md-6">
                            <div class="name">
                                <h4>Lorena <span>Osorio Clavijo</span></h4>
                                <ul>
                                    <li><span class="ion-social-whatsapp ws"></span> (09) 7 624 0100</li>
                                    <li><span class="ion-email mail"></span> lorenaosorio@objetoa.cl</li>
                                    <li><span class="ion-social-linkedin linked"></span> lorenaosorio@objetoa.cl</li>
                                </ul>
                            </div>
                      </div>
                  </div>
              </div>
              <div class="col-xs-12">
                  <div class="row">
                      <div class="col-md-12">
                          <h4>Formación <span>profesional</span></h4>
                      </div>
                  </div>
              </div>
              <div class="col-xs-12">
                  <div class="row">
                      <div class="col-md-12">
                          <h4>Tra<span>yectoria</span></h4>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>





  <div class="modal fade" id="email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content email">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <h2>Deseo <span>me contácten</span></h2>
                <form class="col s12">
                    <div class="row">
                    <div class="input-field col s12">
                        <input id="first_name" type="text" class="validate">
                        <label for="first_name">Nombre</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="last_name" type="text" class="validate">
                        <label for="last_name">Teléfono</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" class="validate">
                        <label for="email">Correo</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <textarea id="textarea1" class="materialize-textarea"></textarea>
                        <label for="textarea1">Textarea</label>
                    </div>
                    </div>
                    <button class="btn btn-primary pull-right" type="submit" name="action">Enviar</button>
                </form>
            </div>
        </div>
      </div>
    </div>
  </div>