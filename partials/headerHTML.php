<!DOCTYPE html>
    <html lang="es">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Objeto A</title>

        

        <!-- Favicon -->
        <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'>
        <link rel='icon' href='img/favicon.ico' type='image/x-icon'>

        <!-- Iconos -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css">

        <!-- Slider -->
        <link href="css/demo.css" rel="stylesheet">
        <link href="css/style3.css" rel="stylesheet">
        <script type="text/javascript" src="js/modernizer.js"></script>

        <!-- Accordeon -->
        <link href="css/my-app.css" rel="stylesheet">

        <!-- materialize -->
        <link rel="stylesheet" href="css/materialize.css">

        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="css/my-app.css" rel="stylesheet">

        

    </head>

    <body>