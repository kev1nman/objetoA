<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">
                <img class="brand2 hide" alt="Brand" src="img/logoNegro.png">
                <img class="brand" alt="Brand" src="img/logoBlanco.png"> Objeto A
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="#porque">¿ Porqué consultar ?</a></li>
                <li><a href="#servicios">Servicios</a></li>
                <li><a href="#profesionales">Profesionales</a></li>
                <li><a href="#contacto">Contacto</a></li>
                <li><a href="#contacto" class="ws"><i class="ion-social-whatsapp"></i> 2 979 8150</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>