<?php include "partials/headerHTML.php"; include "partials/nav.php"; ?>

<section class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 noP">
                <div class="slider" id="page">
                    <ul class="cb-slideshow">
                        <li><span></span><div><h3>re·lax·a·tion</h3></div></li>
                        <li><span></span><div><h3>qui·e·tude</h3></div></li>
                        <li><span></span><div><h3>re·lax·a·tion</h3></div></li>
                        <li><span></span><div><h3>qui·e·tude</h3></div></li>
                        <li><span></span><div><h3>re·lax·a·tion</h3></div></li>
                        <li><span></span><div><h3>qui·e·tude</h3></div></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="sect1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-left"><span class="theme1">Clínica </span>infantil <small>Espacios clínicos</small></h1>
                <div class="separador">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-3 linea1"></div>
                            <div class="col-xs-9 linea2"></div>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <p>
                        Espacio de reflexión a través del juego, el dibujo y el uso libre de la palabra para niños y niñas entre los 3 y 12 años. <br> <br>
                        
                        Escuchar el sufrimiento de un niño implica un conjunto de estrategias que, más allá del exclusivo uso de la palabra, le posibiliten a éste la construcción de un espacio donde pueda poner en (el) juego su malestar. Esto, porque el sufrimiento de un niño, al estar inscrito en la cultura, suele expresarse en los modos en que juega y en el contenido de sus dibujos. Es a través de estas actividades, propias de la infancia, que el niño logra proyectar las razones que sostienen la manifestación de un conjunto de síntomas que afectan su vida personal, tanto en relación a su familia como a las instituciones encargadas de su formación cívica. La tarea del analista, en tal caso, es descifrar junto al niño el sentido de estas razones. Para ello es fundamental dar oídos al niño más allá de la demanda de sus padres. <br><br>
                        
                        Esto no significa desatender a los padres y sus discursos (esto es, los motivos por las que demandan tratamiento para su hijo), por el contrario, la cuestión es escuchar, a través del niño las dificultades que, tanto los padres como las figuras significativas y las instituciones de formación, provocan en él cuando no puede responder a los deseos y exigencias de estos otros. De ahí que la clínica infantil organiza un espacio lúdico para que el niño pueda desplegar su malestar y el sentido de sus síntomas. El camino hacia la cura depende, en consecuencia, de tres elementos fundamentales: 1. las producciones plásticas del sufrimiento a través de la creación de esculturas, dibujos, construcciones con legos, pinturas, etc.); 2. el juego de roles y 3. la construcción de historias mediante las cuales el niño metaforiza su contexto y realidad cotidiana. Es gracias a la dimensión lúdica que el analista accede a las producciones inconscientes infantiles y al posterior análisis de las mismas. El resultado de esta labor es aquello que ha de posibilitar, cada vez, la apertura para una resolución del nudo traumático por el cual ha sido tomado el infante.
                    </p>
                    <a class="btn btn-primary btn-servicio pull-right" href="#" data-toggle="modal" data-target="#email">Deseo me contácten</a>
                </div>
                
            </div>
        </div>
    </div>
</section>


<section class="sect2">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1><span class="theme1">Otros </span>servicios</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box servicio serviciosm">
                <div class="row">
                    <div class="col-md-5">
                        <div class="img2">
                            
                        </div>
                    </div>
                    <div class="col-md-7 info">
                        <h3>Clínica juvenil</h3>
                        <p>
                            Espacio de trabajo clínico, interrogación y reflexión para jóvenes entre 13 y 18 años.                            </p>
                        <p class="indentado">
                            “Dad palabra al dolor: el dolor que no habla gime en el corazón hasta que lo rompe” (William Shakespeare)                            </p>
                        <a class="btn btn-primary btn-servicio" href="servicio.php">Me interesa</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box servicio serviciosm">
                <div class="row">
                    <div class="col-md-5">
                        <div class="img3">

                        </div>
                    </div>
                    <div class="col-md-7 info">
                        <h3>Clínica adultos</h3>
                        <p>
                            Espacio de escucha, interpelación y análisis para mayores de 19 años.
                        </p>
                        <p class="indentado">
                            "Quizá sólo buscamos a lo largo de la vida la gran aflicción de ser uno mismo y nada más" (Louis Céline)                            </p>
                        <a class="btn btn-primary btn-servicio" href="servicio.php">Me interesa</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>






<?php include "modals.php"; include "partials/footer.php"; ?>